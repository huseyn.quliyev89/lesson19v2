package az.ingress.redishashcachingv2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Redishashcachingv2Application {

    public static void main(String[] args) {
        SpringApplication.run(Redishashcachingv2Application.class, args);
    }

}
