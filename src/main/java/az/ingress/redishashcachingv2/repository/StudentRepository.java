package az.ingress.redishashcachingv2.repository;


import az.ingress.redishashcachingv2.model.Student;
import org.springframework.data.repository.CrudRepository;

import java.util.List;


public interface StudentRepository extends CrudRepository<Student, Integer> {
       List<Student> findAllByName(String name);
    }
