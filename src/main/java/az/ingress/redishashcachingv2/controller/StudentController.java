package az.ingress.redishashcachingv2.controller;

import az.ingress.redishashcachingv2.model.Student;
import az.ingress.redishashcachingv2.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1")
public class StudentController {
    private final StudentService studentService;

    @GetMapping("/student/{id}")
    public Student getStudentById(@PathVariable Integer id) {
        return studentService.findById(id);
    }

    @GetMapping("/student/name/{name}")
    public List<Student> getStudentByName(@PathVariable String  name) {
        return studentService.findByName(name);
    }

    @GetMapping("/student")
    public List<Student> getStudents() {
        return studentService.findAll();
    }

    @DeleteMapping("/student/{id}")
    public void deleteStudent(@PathVariable Integer id) {
        studentService.deleteStudent(id);
    }

    @PutMapping("/student/{id}")
    public Student updateStudent(@PathVariable Integer id, @RequestBody Student student) {
        return studentService.updateStudent(id,student);
    }

    @PostMapping("/student")
    public Student saveStudent(@RequestBody Student student) {
        return studentService.saveStudent(student);
    }
}
