package az.ingress.redishashcachingv2.service;

import az.ingress.redishashcachingv2.model.Student;

import java.util.List;

public interface StudentService {
    Student saveStudent(Student student);
    Student updateStudent(Integer id, Student student);
    void deleteStudent(Integer id);
    Student findById(Integer id);
    List<Student> findAll();
    List<Student> findByName(String name);

}
