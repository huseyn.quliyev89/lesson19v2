package az.ingress.redishashcachingv2.service;


import az.ingress.redishashcachingv2.model.Student;
import az.ingress.redishashcachingv2.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService{

    private final StudentRepository studentRepository;
    @Override
    public Student saveStudent(Student student) {
        return studentRepository.save(student);
    }

    @Override
    public Student updateStudent(Integer id, Student student) {
        studentRepository.findById(id).orElseThrow(RuntimeException::new);
        student.setId(id);
        return studentRepository.save(student);
    }

    @Override
    public void deleteStudent(Integer id) {
      studentRepository.deleteById(id);
    }

    @Override
    public Student findById(Integer id) {
        return studentRepository.findById(id).orElseThrow(RuntimeException::new);
    }

    @Override
    public List<Student> findAll() {
        return (List<Student>) studentRepository.findAll();

    }

    @Override
    public List<Student> findByName(String name) {
        return studentRepository.findAllByName(name);
    }
}
